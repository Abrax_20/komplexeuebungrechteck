package controller;

import model.MysqlDatabase;
import model.Rechteck;
import view.RechteckInputGUI;

import java.util.LinkedList;

public class BunteRechteckeController {
    private MysqlDatabase mysqlDatabase;
    private LinkedList<Rechteck> rechtecke;
    private RechteckInputGUI rechteckInputGUI;

    public void add(Rechteck rechteck) {
        this.rechtecke.add(rechteck);
        this.mysqlDatabase.rechteckEintragen(rechteck);
    }


    public void reset() {
        this.rechtecke = new LinkedList<Rechteck>();
    }

    public BunteRechteckeController() {
        this.rechtecke = new LinkedList<Rechteck>();
        this.mysqlDatabase = new MysqlDatabase();
        this.rechtecke = this.mysqlDatabase.getAllRectangle();
    }

    @Override
    public String toString() {
        String string = "BunteRechteckeController [rechtecke=[";
        for (int i = 0; i < this.rechtecke.size(); i++) {
            string = string + this.rechtecke.get(i).toString() + (i+1 != this.rechtecke.size() ? ", " : "");
        }

        string = string + "]]";
        return string;
    }

    public LinkedList<Rechteck> getRechtecke() {
        return rechtecke;
    }

    public void setRechtecke(LinkedList<Rechteck> rechtecke) {
        this.rechtecke = rechtecke;
    }

    public static void main(String[] args) {}

    public void showRechteckeInputModal() {
        rechteckInputGUI = new RechteckInputGUI(this);
        rechteckInputGUI.setVisible(true);
    }

    public void closeBunteRechteckeGUI() {
        rechteckInputGUI.setVisible(false);
        rechteckInputGUI = null;
    }
}
