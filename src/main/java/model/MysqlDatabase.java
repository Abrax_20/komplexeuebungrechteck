package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MysqlDatabase {
    public static final String driver = "com.mysql.cj.jdbc.Driver";
    public static final String uri = "jdbc:mysql://localhost/rechtecke?";
    public static final String user = "root";
    public static final String password = "root";

    public LinkedList<Rechteck> getAllRectangle() {
        LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();
        try {
            Class.forName(this.driver);
            Connection con = DriverManager.getConnection(this.uri, this.user, this.password);
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM T_Rechtecke;");
            while (resultSet.next()) {
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");
                int width = resultSet.getInt("width");
                int height = resultSet.getInt("height");
                rechtecke.add(new Rechteck(x, y, width, height));
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rechtecke;
    }

    public void rechteckEintragen(Rechteck r) {
        try {
            Class.forName(this.driver);
            Connection con = DriverManager.getConnection(this.uri, this.user, this.password);
            PreparedStatement ps = con.prepareStatement("INSERT INTO T_Rechtecke(x,y,width, height) VALUES(?,?,?,?);");
            ps.setInt(1, r.getX());
            ps.setInt(2, r.getY());
            ps.setInt(3, r.getBreite());
            ps.setInt(4, r.getHoehe());
            ps.executeUpdate();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
