package model;

public class Punkt {
    private int x = 0;
    private int y = 0;

    public Punkt() {}

    public Punkt (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = Math.abs(x);
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        Math.abs(this.y = y);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Punkt) {
            if (((Punkt) obj).getX() == this.x && ((Punkt) obj).getY() == this.y) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString () {
        return "Punkt [x=" + this.x + ", y=" + this.y + "]";
    }
}
