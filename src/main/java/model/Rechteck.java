package model;

import java.util.Random;

public class Rechteck {
    private int hoehe;
    private int breite;
    private Punkt punkt = new Punkt();

    public Rechteck() {
        this.hoehe = 0;
        this.breite = 0;
    }

    public Rechteck(int x, int y, int breite, int hoehe) {
        this.setX(x);
        this.setY(y);
        this.setBreite(breite);
        this.setHoehe(hoehe);
    }

    public Rechteck(Punkt punkt, int breite, int hoehe) {
        this.setX(punkt.getX());
        this.setY(punkt.getY());
        this.setBreite(breite);
        this.setHoehe(hoehe);
    }


    public void setX(int x) {
        punkt.setX(x);
    }

    public int getX() {
        return punkt.getX();
    }

    public void setY(int y) {
        punkt.setY(y);
    }

    public int getY() {
        return punkt.getY();
    }

    public void setHoehe(int hoehe) {
        this.hoehe = Math.abs(hoehe);
    }

    public int getHoehe() {
        return hoehe;
    }

    public void setBreite(int breite) {
        this.breite = Math.abs(breite);
    }

    public int getBreite() {
        return breite;
    }

    public boolean enthaelt(Rechteck rechteck) {
        return this.enthaelt(rechteck.getX(), rechteck.getY()) && this.enthaelt(punkt.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
    }

    public boolean enthaelt(Punkt punkt) {
        return this.enthaelt(punkt.getX(), punkt.getY());
    }

    public boolean enthaelt(int x, int y) {
        if (
            x >= this.getX() && x <= this.getX() + this.getBreite() &&
            y >= this.getY() && y <= (this.getY() + this.getHoehe())
        ) {
            return true;
        }

        return false;
    }

    public static Rechteck generiereZufallsRechteck() {
        Random randomGenerator = new Random();
        int x = randomGenerator.nextInt(1200);
        int y = randomGenerator.nextInt(1000);
        int width = randomGenerator.nextInt(1200-x);
        int height = randomGenerator.nextInt(1000-y);
        return new Rechteck(x, y, width, height);
    }

    @Override
    public String toString() {
        return "Rechteck [x=" + String.valueOf(this.punkt.getX()) + ", y=" + String.valueOf(this.punkt.getY()) + ", breite=" + String.valueOf(this.breite) +  ", hoehe=" + String.valueOf(this.hoehe) + "]";
    }
}
