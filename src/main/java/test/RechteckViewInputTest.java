package test;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.RechteckInputGUI;

import javax.swing.*;

@SuppressWarnings("serial")
public class RechteckViewInputTest {

    private JPanel contentPane;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        BunteRechteckeController brc = new BunteRechteckeController();
        brc.add(new Rechteck(330, 330, 50, 50));
        brc.add(new Rechteck(380, 380, 50, 50));
        brc.add(new Rechteck(440, 440, 50, 50));
        brc.add(new Rechteck(500, 500, 50, 50));
        brc.add(new Rechteck(560, 440, 50, 50));
        brc.add(new Rechteck(620, 380, 50, 50));
        brc.add(new Rechteck(680, 330, 50, 50));
        brc.add(new Rechteck(740, 270, 50, 50));
        brc.add(new Rechteck(800, 210, 50, 50));
        brc.add(new Rechteck(860, 150, 50, 50));
        RechteckInputGUI rechteckInputGUI = new RechteckInputGUI(brc);
        rechteckInputGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
