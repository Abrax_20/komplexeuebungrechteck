package view;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BunteRechteckeGUI extends JFrame {
    private JPanel contentPain;
    private JMenuBar menuBar;
    private JMenu menu;
    private BunteRechteckeController brc;
    private JMenuItem menuItemNeuesRechteck;

    public static void main(String[] args) {
        try {
            BunteRechteckeGUI frame = new BunteRechteckeGUI();
            frame.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void run() throws InterruptedException {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.revalidate();
            this.repaint();
        }
    }

    public BunteRechteckeGUI() {
        this.brc = new BunteRechteckeController();
        this.setBounds(0, 0, Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
        this.contentPain = new Zeichenflaeche(this.brc);
        this.contentPain.setBorder(new EmptyBorder(5,5,5,5));
        this.contentPain.setLayout(new BorderLayout());
        this.setContentPane(contentPain);
        this.menuBar = new JMenuBar();
        this.setJMenuBar(this.menuBar);
        this.menu = new JMenu("Hinzufügen");
        this.menuBar.add(this.menu);
        this.menuItemNeuesRechteck = new JMenuItem("Rechteck Hinzufügen");
        this.menu.add(this.menuItemNeuesRechteck);
        this.menuItemNeuesRechteck.addActionListener(e -> this.brc.showRechteckeInputModal());
        this.setVisible(true);
    }
}
