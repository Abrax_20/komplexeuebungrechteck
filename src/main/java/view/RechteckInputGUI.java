package view;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.*;
import java.awt.*;

public class RechteckInputGUI extends JFrame {
    private BunteRechteckeController bunteRechteckeController;
    private JTextField x = new JTextField();
    private JTextField y = new JTextField();
    private JTextField height = new JTextField();
    private JTextField width = new JTextField();

    public RechteckInputGUI(BunteRechteckeController bunteRechteckeController) {
        this.bunteRechteckeController = bunteRechteckeController;

        GridLayout gridLayout = new GridLayout(0,1);
        this.setLayout(gridLayout);



        x.setSize(new Dimension(100, 100));
        y.setSize(new Dimension(100, 100));
        height.setSize(new Dimension(100, 100));
        width.setSize(new Dimension(100, 100));

        this.add(new JLabel("x"));
        this.add(x);
        this.add(new JLabel("y"));
        this.add(y);
        this.add(new JLabel("height"));
        this.add(height);
        this.add(new JLabel("width"));
        this.add(width);
        JButton button = new JButton("Hinzufügen");
        this.add(button);

        button.addActionListener((e) -> {
            this.bunteRechteckeController.add(
                    new Rechteck(
                            Integer.parseInt(this.x.getText()),
                            Integer.parseInt(this.y.getText()),
                            Integer.parseInt(this.width.getText()),
                            Integer.parseInt(this.height.getText())));
            this.bunteRechteckeController.closeBunteRechteckeGUI();
        });

        this.pack();
        this.setVisible(true);
    }
}
