package view;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.*;
import java.awt.*;

public class Zeichenflaeche extends JPanel {
    private BunteRechteckeController bunteRechteckeController;

    public Zeichenflaeche(BunteRechteckeController bunteRechteckeController) {
        this.bunteRechteckeController = bunteRechteckeController;
    }

    @Override
    protected void paintComponent(Graphics g) {
        for (Rechteck rechteck : this.bunteRechteckeController.getRechtecke()) {
            g.setColor(Color.BLACK);
            g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
        }

    }
}
